﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage1.Master" AutoEventWireup="true" CodeBehind="Gallery.aspx.cs" Inherits="WebApplication1.Gallery" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
<section id="mainGallery">
    <section id="textGallery"><h1>Gallery</h1></section>
    <section id="gallery">
            <!-- thumbnail image wrapped in a link -->
            <a href="#img1">
              <img src="images/zeb.jpg" class="thumbnail">
            </a>

            <!-- lightbox container hidden with CSS -->
            <a href="#_" class="lightbox" id="img1">
              <img src="images/zeb.jpg">
            </a>
    

                <!-- thumbnail image wrapped in a link -->
            <a href="#img2">
              <img src="images/gazelle.jpg" class="thumbnail">
            </a>

            <!-- lightbox container hidden with CSS -->
            <a href="#_" class="lightbox" id="img2">
              <img src="images/gazelle.jpg">
            </a>


               <!-- thumbnail image wrapped in a link -->
            <a href="#img3">
              <img src="images/lion.jpg" class="thumbnail">
            </a>

           <!-- lightbox container hidden with CSS -->
            <a href="#_" class="lightbox" id="img3">
              <img src="images/lion.jpg">
            </a>






                <!-- thumbnail image wrapped in a link -->
            <a href="#img4">
              <img src="images/pentere.jpg" class="thumbnail">
            </a>

            <!-- lightbox container hidden with CSS -->
            <a href="#_" class="lightbox" id="img4">
              <img src="images/pentere.jpg">
            </a>


                <!-- thumbnail image wrapped in a link -->
            <a href="#img5">
              <img src="images/zeb.jpg" class="thumbnail">
            </a>

            <!-- lightbox container hidden with CSS -->
            <a href="#_" class="lightbox" id="img5">
              <img src="images/zeb.jpg">
            </a>


        
                <!-- thumbnail image wrapped in a link -->
            <a href="#img6">
              <img src="images/zebra.jpg" class="thumbnail">
            </a>

            <!-- lightbox container hidden with CSS -->
            <a href="#_" class="lightbox" id="img6">
              <img src="images/zebra.jpg">
            </a>


        
                <!-- thumbnail image wrapped in a link -->
            <a href="#img7">
              <img src="images/Syncerus_caffer.jpg" class="thumbnail">
            </a>

            <!-- lightbox container hidden with CSS -->
            <a href="#_" class="lightbox" id="img7">
              <img src="images/Syncerus_caffer.jpg">
            </a>


        
                <!-- thumbnail image wrapped in a link -->
            <a href="#img8">
              <img src="images/lion2.jpg" class="thumbnail">
            </a>

            <!-- lightbox container hidden with CSS -->
            <a href="#_" class="lightbox" id="img8">
              <img src="images/lion2.jpg">
            </a>


        </section>
</section>

</asp:Content>
