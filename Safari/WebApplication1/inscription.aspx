﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage1.Master" AutoEventWireup="true" CodeBehind="inscription.aspx.cs" Inherits="WebApplication1.inscription" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

            <div class="container">
  <h2>Inscription</h2>
  <form class="form-horizontal" action="insc.aspx">
   
    <div class="form-group">
      <label class="control-label col-sm-2" >Nom:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="nom" placeholder="nom" name="nom" required>
      </div>
    </div>

      <div class="form-group">
      <label class="control-label col-sm-2" >Prenom:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="prenom" placeholder="Prenom" name="prenom" required>
      </div>
    </div>
    
      <div class="form-group">
      <label class="control-label col-sm-2" >Tel:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="tel" placeholder="Tel" name="tel" required>
      </div>
    </div>
    
      <div class="form-group">
      <label class="control-label col-sm-2" >Email:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" id="email" placeholder="Email" name="email" required>
      </div>
    </div>
     
      <div class="form-group">
      <label class="control-label col-sm-2" >Trip:</label>
      <div class="col-sm-10">          
        <input type="text" class="form-control" value="<%Response.Write(trip());%>" id="trip" placeholder="trip" name="trip" required>
      </div>
    </div>

   
    <div class="form-group">        
      <div class="col-sm-offset-2 col-sm-10">
        <button type="submit" class="btn btn-success">Inscription</button>
      </div>
    </div>
  </form>
</div>


</asp:Content>
