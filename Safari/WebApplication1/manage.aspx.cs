﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class manage : System.Web.UI.Page
    {

        private SqlConnection con;
        private SqlCommand cmd;
        private SqlDataReader reader;

        protected void Page_Load(object sender, EventArgs e)
        {

            string id = (Request["idd"].ToString().Trim());
            string entete = (Request["entete"].ToString().Trim());
            string titre = (Request["titre"].ToString().Trim());
            string desc = (Request["desc"].ToString().Trim());
            string vedette = (Request["vedette"].ToString().Trim());
            string type = (Request["type"].ToString().Trim());



            int result = 0;
            this.con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Karim\source\repos\Safari\WebApplication1\App_Data\DatabaseProduit.mdf;Integrated Security=True");
            try
            {

                if (type == "ajouter")
                {
                    this.cmd = new SqlCommand("insert into trips(tete,titre,descr)values(@e,@t,@d);", this.con);
                    cmd.Parameters.AddWithValue("@t", titre);
                    cmd.Parameters.AddWithValue("@e", entete);
                    cmd.Parameters.AddWithValue("@d", desc);

                    this.con.Open();
                    result = cmd.ExecuteNonQuery();

                }
                else if (type == "delete")
                {

                    //string id = (Request["idd"].ToString().Trim());
                    this.cmd = new SqlCommand("delete from trips where Id=@i", this.con);
                    cmd.Parameters.AddWithValue("@i", id);
             

                    this.con.Open();
                    result = cmd.ExecuteNonQuery();

                }
                else
                {

                    this.cmd = new SqlCommand("update trips set tete=@e,titre=@t,descr=@d,vedette=@v where id=@id", this.con);
                    cmd.Parameters.AddWithValue("@t", titre);
                    cmd.Parameters.AddWithValue("@e", entete);
                    cmd.Parameters.AddWithValue("@d", desc);
                    cmd.Parameters.AddWithValue("@id", id);
                    cmd.Parameters.AddWithValue("@v", vedette);

                    this.con.Open();
                    result = cmd.ExecuteNonQuery();
                }



               




            }
            catch (Exception ex)
            {

                Response.Write(ex.Message);
            }
            finally
            {
                con.Close();
                Response.Write("<script> document.location.href ='trips.aspx';</script>");
            }

        }
    }
}