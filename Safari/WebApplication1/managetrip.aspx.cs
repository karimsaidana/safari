﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebApplication1
{
    public partial class managetrip : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        public string idd()
        {
            return (Request["id"].ToString().Trim());
        }
        
        public string entete()
        {
            return (Request["entete"].ToString().Trim());
        }

        public string titre()
        {
            return(Request["titre"].ToString().Trim());
        }
        public string desc()
        {
            return (Request["desc"].ToString().Trim());
        }

        public string type()
        {
            return (Request["type"].ToString().Trim());
        }
    }
}