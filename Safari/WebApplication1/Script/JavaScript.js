﻿
$(document).ready(function () {
    
    $('#show').fadeIn(1000);
    var x = 0;
    setInterval(function () {
        
        if (x == 1) {
            $('#show').fadeOut('fast', function () {
                $('#show').attr('src', 'images/tigre.png');
                $('#show').fadeIn('fast');
            });
        }
        else if (x == 2) {
            $('#show').fadeOut('fast', function () {
                $('#show').attr('src', 'images/lion2.jpg');
                $('#show').fadeIn('fast');
            });
        }
        else
        {
            $('#show').fadeOut('fast', function () {
                $('#show').attr('src', 'images/savana.jpg');
                $('#show').fadeIn('fast');
            });
            x = 0;
        }
        x++;
    }, 2000);

});

//////////////////////Ajax Changer dynamiquement de la langue

$(document).ready(function () {



    getData(1);
   

    $('.btnlangue').each(function () {
        $(this).click(function () {
            var id = $(this).attr('id');
           // alert(id);

            getData(id);

          

        });
    });
   
});


function getData(index){


    $.get("langue.aspx?cmd=" + index, function (data) {
   
        var traduction = JSON.parse(data);

        $("#headCarte").text(traduction.headCarte);

        $("#headCarte1").text(traduction.headCarte1);
        $("#headCarte2").text(traduction.headCarte2);
        $("#titreP").text(traduction.titreP);
        $("#titreP1").text(traduction.titreP1);
        $("#titreP2").text(traduction.titreP2);
        $("#textP").text(traduction.textP);
        $("#textP1").text(traduction.textP1);
        $("#textP2").text(traduction.textP2);
        $("#More").text(traduction.More);
        $("#More2").text(traduction.More);
        $("#More3").text(traduction.More);

        $("#M1").text(traduction.M1);
        $("#M2").text(traduction.M2);
        $("#M3").text(traduction.M3);
        $("#M4").text(traduction.M4);
        $("#M5").text(traduction.M5);
        $("#M6").text(traduction.M6);

        $("#globe").text(traduction.globe);


    });


}

$(document).ready(function () {
//script pour la page contact



    $('#btnSend').click(function () {



        var nom = $('#txtName').val();
        var email = $('#txtEmail').val();
        var tel = $('#txtPhone').val();
        var message = $('#txtMessage').val();


        if (validate()) {


            $.get("Info.aspx?nom=" + nom + "&email=" + email + "&tel=" + tel + "&message=" + message, function (data) {
                if (data == "1") {
                    $('.error').css('color', 'green');
                    $('.error').text(data);

                }
                else {

                    $('.error').text(data);

                }
            });
        }
       





    });
  














function  validate() {
    var nom = $('#txtName').val();
    var email = $('#txtEmail').val();
    var tel = $('#txtPhone').val();
    var message = $('#txtMessage').val();

    var c1 = new contact(nom, email, tel, message);

    if (c1.validateNom() && c1.validateEmail() && c1.validateTel() && c1.validateMessage()) {
      
        return true;
    }
    else {
        //nom.style.borderColor = "red";
        //tel.style.borderColor = "red";
        //email.style.borderColor = "red";
        return false;

    }

   

}
//////////////////////////////////////////////////////////////////////////////
/////////// envoyer les infos de contact au cote serveur



//création du constructeur de la classe contact
function contact(nom,email, tel, message) {
    this.nom = nom;  
    this.email = email;
    this.tel = tel;
    this.message = message;

}
//propriétée de la classe contact qui
//permet de réccupérer le nom
contact.prototype.getNom = function () {
    return this.nom;
}
//propriétée de la classe contact qui
//permet de modifier le nom
contact.prototype.setNom = function (nom) {
    this.nom = nom;
}


//propriétée de la classe contact qui
//permet de réccupérer le mail
contact.prototype.getMail = function () {
    return this.email;
}
//propriétée de la classe contact qui
//permet de modifier le mail
contact.prototype.setMail = function (email) {
    this.email = email;

}

//propriétée de la classe contact qui
//permet de modifier le tel
contact.prototype.setTel = function (tel) {

    this.tel = tel;

}
//propriétée de la classe contact qui
//permet de réccupérer le tel
contact.prototype.getTel = function () {
    return this.tel;
}
//propriétée de la classe contact qui
//permet de modifier la message
contact.prototype.setMessage = function (message) {

    this.message = message;

}
//propriétée de la classe contact qui
//permet de réccupérer la date
contact.prototype.getMessage = function () {

    return this.message;

}
//fonction permettant de
//ng valider le nom
contact.prototype.validateNom = function () {
   
    var nomRegEx =/^[a-zA-Z]+[ \-]?[a-zA-Z]+$/;
    if (nomRegEx.test(this.nom)) {
        return true;  //code
    }
    else {

        $('#txtName').css('border-color', 'red');
        $('.error').text("le nom est invalide!!!");
      return false;
    }









}

//fonction permettant de valider message
    contact.prototype.validateMessage = function () {
        regExMessage =/^.{5,100}$/;
   if (regExMessage.test(this.message)) {
            return true;
        }
   else {
       $('#txtMessage').css('border-color', 'red');
       $('.error').text("vous avez dépassé 100 caractere!!!");
       return false;
        }



   
    }

//fonction permettant de valider le numéro de tel
    contact.prototype.validateTel = function () {


        var telRegEx = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
        if (telRegEx.test(this.tel)) {
            return true;  //code
        }
        else {
            $('#txtPhone').css('border-color', 'red');
            $('.error').text("Numero de telephone n' est pas le format Nord Americain!!!");

            return false;
           
        }





}
//fonction permettant de valider le email
contact.prototype.validateEmail = function () {

    var emailRegEx = /^[a-zA-Z0-9]+(\.|_|-)?[a-zA-Z0-9]+@[a-zA-Z]+\.(ca|fr|com|de)$/;
  
                   
    if (emailRegEx.test(this.email)) {
        return true;  //code
    }
  else
    {

        $('#txtEmail').css('border-color', 'red');
        $('.error').text("Email ne respecte pa la forme toto@too.ca /fr/de!!!");
        return false;
    }


}











    $('#btnClear').click(function () {
       

        $('#txtName').val('');
        $('#txtEmail').val('');
        $('#txtPhone').val('');
        $('#txtMessage').val('');
        $('#txtName').css('border-color', 'initial');
        $('#txtEmail').css('border-color', 'initial');
        $('#txtPhone').css('border-color', 'initial');
        $('#txtMessage').css('border-color', 'initial');


    });

 

    });


