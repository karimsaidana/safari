﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage1.Master" AutoEventWireup="true" CodeBehind="managetrip.aspx.cs" Inherits="WebApplication1.managetrip" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">

    
            <div class="container">
  <h2>Manage Trip</h2>
  <form class="form-horizontal" action="manage.aspx">
   
    <input class="idd" id="idd" name="idd" type="hidden" value="<%if(type()=="update") Response.Write(idd());else Response.Write("");%>">
   
      <div class="form-group">
      <label class="control-label col-sm-2" >Entete: </label>
      <div class="col-sm-10">          
        <input value="<%if(type()=="update") Response.Write(entete()); %>" type="text" class="form-control" id="entete" placeholder="Entete" name="entete" required>
      </div>
    </div>

      <div class="form-group">
      <label class="control-label col-sm-2" >Titre:</label>
      <div class="col-sm-10">          
        <input value="<%if(type()=="update") Response.Write(titre()); %>"  type="text" class="form-control" id="titre" placeholder="Titre" name="titre" required>
      </div>
    </div>
    
      <div class="form-group">
      <label class="control-label col-sm-2" >Description:</label>
      <div class="col-sm-10">          
          <textarea   class="form-control" id="desc" placeholder="Description" name="desc" required><%if(type()=="update") Response.Write(desc()); %></textarea>
   
      </div>
    </div>
    
      <div class="form-group">
      <label class="control-label col-sm-2" >En vedette:</label>
      <div class="col-sm-10">          
      
      <select class="form-control" id="vedette" placeholder="vedette" name="vedette" required>
      <option value="0">Yes</option>
      <option value="1">No</option>
      </select>
      
      </div>
    </div>
                        
                               
    <div class="form-group">          
      <div class="col-sm-offset-2 col-sm-10">
          <%if (type() == "add")       
              { %>
        <button name="type" value="ajouter" type="submit" class="btn btn-success">Ajouter</button>
          <%} %>
          <%else{ %>
          <button name="type" value="update" type="submit" class="btn btn-success">Update</button>
          <button name="type" value="delete" type="submit" class="btn btn-success">Delete</button>

          <%} %>
      </div>
    </div>
  
  </form>
</div>



</asp:Content>
